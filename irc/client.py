#!/usr/bin/env python

""" client.py: IRC classes to manage an IRC client. """

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


__author__      = "Rafa Couto"
__copyright__   = "Copyright 2012, aplicacionesYredes.com"
__credits__     = ["Rafa Couto"]
__license__     = "GPLv3"
__version__     = "0.1"
__maintainer__  = "Rafa Couto"
__email__       = "rafa@aplicacionesyredes.com"


import socket
import asyncore
import asynchat
from core import EventHook, IrcMessage
import logging
import threading
import time


MESSAGE_SEPARATOR = "\r\n"
SOCKET_TIMEOUT = 30


class IrcClientException(Exception):
	pass


class IrcClient(asynchat.async_chat):

	def __init__(self):

		self._isConnected = False
		self._secondsPing = 30
		self._joinedChannels = {}

		self._onConnect = EventHook()
		self._onDisconnect = EventHook()
		self._onMessage = EventHook()
		self._onNickname = EventHook()

		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		s.settimeout(SOCKET_TIMEOUT)

		asynchat.async_chat.__init__(self, s)
		self.set_terminator(MESSAGE_SEPARATOR)


	@property
	def isConnected(self):
		return self._isConnected


	@property
	def username(self):
		return self._username


	@property
	def nickname(self):
		return self._nickname


	@property
	def realname(self):
		return self._realname


	@property
	def serverHost(self):
		return self._serverHost


	@property
	def serverPort(self):
		return self._serverPort


	@property
	def secondsPing(self):
		return self._secondsPing


	@secondsPing.setter
	def secondsPing(self, seconds):
		self._secondsPing = seconds


	@property
	def joinedChannels(self):
		return self._joinedChannels


	def connect(self, serverHost, serverPort):
		self._serverHost = serverHost
		self._serverPort = serverPort
		asyncore.dispatcher.connect(self, (serverHost, serverPort))


	def disconnect(self):
		self._isConnected = False
		self._socket.close()		


	def register(self, username, password, nickname, realname):
		
		self._username = username
		self._password = password
		self._nickname = nickname
		self._realname = realname

		if password:
			self._messageToServer(IrcMessage(None, "PASS", [password]))

		self._messageToServer(IrcMessage(None, "NICK", [nickname]))

		userparams = [username, "8", "*", ":" + realname]
		self._messageToServer(IrcMessage(None, "USER", userparams))


	def quit(self, message):
		pass


	def oper(self, nickname, password):
		pass


	def userMode(self, mode, enable):
		pass


	def sendPrivMessage(self, user, message):
		pass


	def getChannels(self, mask):
		pass


	def joinChannel(self, name):
		pass


	# events

	@property
	def onConnect(self):
		return self._onConnect


	@property
	def onDisconnect(self):
		return self._onDisconnect


	@property
	def onMessage(self):
		return self._onMessage


	@property
	def onNickname(self):
		return self._onNickname


	# implementation

	def _messageFromServer(self, text):
		
		logging.debug("<-- " + text)
		message = IrcMessage.fromStr(text)
		self._lastPing = time.time()

		if message.command == "PING":
			# process pings from server
			pinger = message.params[1] if len(message.params) >= 2 else None
			logging.info("PING? PONG!")
			self._messageToServer(IrcMessage(None, "PONG", [self._nickname, pinger]))

		elif message.command == "NICK":
			# assigned nick by server
			self._nickname = message.params[0]
			logging.info("NICKname was assigned: " + self._nickname)
			self._onNickname.fire(self, self._nickname)

		else:
			self._onMessage.fire(self, message)


	def _messageToServer(self, message):
		msg = str(message)
		if len(msg) > 512:
			raise IrcClientException("Messages can not exceed 512 chars long.")
		self.push(msg + MESSAGE_SEPARATOR)
		self._lastPing = time.time()
		logging.debug("--> " + msg)


	def testPing(self):		
		if self.isConnected:
			if time.time() > self._lastPing + self._secondsPing:
				self._messageToServer(IrcMessage(None, "PING", [self._serverHost]))



	# asynchat.async_chat implementation

	def collect_incoming_data(self, data):
		'''Buffer the data'''
		self._buffer += data

	def found_terminator(self):
		'''Process a message'''
		message = self._buffer
		self._buffer = ""
		self._messageFromServer(message)


	# asyncore.dispatcher implementation

	def handle_connect(self):
		'''Socket is connected'''
		self._buffer = ""
		self._isConnected = True
		Pinger.addClient(self)
		self._lastPing = time.time()
		logging.info("Connection %s:%i is opened", self._serverHost, self._serverPort)
		self._onConnect.fire(self)

	def handle_close(self):
		'''Socket is closed'''
		self._isConnected = False
		Pinger.removeClient(self)
		asyncore.dispatcher.close(self)
		logging.info("Connection %s:%i was closed.", self._serverHost, self._serverPort)
		self._onDisconnect.fire(self)


class Pinger(threading.Thread):

	_clients = []

	def __init__(self):
		threading.Thread.__init__(self)

	def run(self):
		self._stop = False
		while not self._stop:
			asyncore.loop(timeout=1, count=1)
			for client in Pinger._clients:
				client.testPing()

	def stop(self):
		self._stop = True

	@staticmethod
	def addClient(client):
		Pinger._clients.append(client)

	@staticmethod
	def removeClient(client):
		if client in Pinger._clients:
			Pinger._clients.remove(client)


class Channel:

	def __init__(self, name, topic):

		self._name = name
		self._topic = topic
		self._users = []
		self._parted = False


	@property
	def name(self):
		return self._name


	@property
	def topic(self):
		return self._topic


	@property
	def users(self):
		return self._users


	def sendMessage(self, message):
		if self._parted:
			raise IrcClientException("Impossible to send messages to a parted channel.")
		pass


	def part(self):
		if self._parted: return
		pass



def main():

	logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', datefmt='%Y-%m-%d %H:%M:%S', level=logging.DEBUG)
	
	client = IrcClient()
	client.onConnect += client_onConnect
	client.onDisconnect += client_onDisconnect
	client.onMessage += client_onMessage
	client.onNickname += client_onNickname
	client.connect("localhost", 6667)

	try:
		pinger = Pinger()
		pinger.start()
		raw_input("Press <enter> to stop client: ")
	finally:
		pinger.stop()


def client_onConnect(client):
	client.register("pybot", None, "pybot8767", "IrcBot " + __version__)


def client_onDisconnect	(client):
	print "See you later!"


def client_onMessage(sender, message):
	print "From server: %s %s" % (message.command, message.params)


def client_onNickname(sender, nickname):
	print "Assigned nickname: " + nickname


if __name__ == "__main__":
    main()
